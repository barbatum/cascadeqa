/****************************************************************************************************
This code is part of the code supplied with the OpenCV Blueprints book.
It was written by Steven Puttemans, who can be contacted via steven.puttemans[at]kuleuven.be

License can be found at https://github.com/OpenCVBlueprints/OpenCVBlueprints/blob/master/license.txt
*****************************************************************************************************
This software can be used for comparing CPU and GPU object detection performance on a given sample
****************************************************************************************************/

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/cudaobjdetect.hpp>

using namespace std;
using namespace cv;

#define OPENCV_CASCADE_CUDA_FILENAME "data/haarcascades_cuda/haarcascade_fullbody.xml"
#define OPENCV_CASCADE_FILENAME "data/haarcascades/haarcascade_fullbody.xml"
#define OPENCV_TEST_IMAGE "data/example.jpg"

void cpuTest(const char *inFilename, Mat &hist) {
    // Perform the CPU detector when downscaling 5 times
    cerr << "---------------------------------------------------------------------" << endl;
    cerr << "CPU PROCESSING - each time 10 images processed" << endl;
    cerr << "File: " << inFilename << endl;
    cerr << "---------------------------------------------------------------------" << endl;
    CascadeClassifier cpu_detector(inFilename);
    if(cpu_detector.empty()){
        cerr << "NOT LOADED";
        return;
    }
    for(int scale = 1; scale<6; scale++){
        Mat current;
        resize(hist, current, Size(hist.rows/scale, hist.cols/scale));
        // Start timing here
        int64 t0 = getTickCount();
        for(int j=0; j<10; j++){
            vector<Rect> objects;
            cpu_detector.detectMultiScale(current, objects, 1.1, 0);
        }
        // End timing here and output
        int64 t1 = getTickCount();
        double secs = (t1-t0)/getTickFrequency();
        cerr << "Image dimensions = [" << current.rows << " " << current.cols << "]" << endl;
        cerr << "Measurement - division by " << scale << ": time = " << secs << " seconds"<< endl;
    }
}

int main()
{
    // Read in an original image
    // Already apply processing
    Mat image = imread(OPENCV_TEST_IMAGE, IMREAD_GRAYSCALE);
    Mat hist;
    equalizeHist(image, hist);

    cpuTest(OPENCV_CASCADE_FILENAME, hist);

    cpuTest(OPENCV_CASCADE_CUDA_FILENAME, hist);

    // Set the CUDA device to avoid this influencing the time calculations
    cuda::setDevice(0);

    cerr << "---------------------------------------------------------------------" << endl;
    cerr << "GPU PROCESSING - each time 10 images processed" << endl;
    cerr << "---------------------------------------------------------------------" << endl;
    // Perform the GPU detector
    Ptr<cuda::CascadeClassifier> cascade_gpu = cuda::CascadeClassifier::create(OPENCV_CASCADE_CUDA_FILENAME);
    cascade_gpu->setScaleFactor(1.1);
    cascade_gpu->setMinNeighbors(0);
    // Since my GPU NVIDIA Quadro K2000 has only 1GB of dedicated memory I cannot process more than 4000x4000 pixels in a single run.
    // Therefore we need to split the image, and do a double run, and thus a double pushing from CPU<->GPU
    for(int scale = 2; scale<6; scale++){
        Mat current;
        resize(hist, current, Size(image.rows/scale, image.cols/scale));
        // Start timing here
        int64 t0 = getTickCount();
        // We need to include the time for pushing and retrieving the data to and from the GPU
        for(int j=0; j<10; j++){
            cuda::GpuMat image_gpu(current);
            cuda::GpuMat objbuf;
            cascade_gpu->detectMultiScale(image_gpu, objbuf);
            std::vector<Rect> detections;
            cascade_gpu->convert(objbuf, detections);
        }
        // End timing here and output
        int64 t1 = getTickCount();
        double secs = (t1-t0)/getTickFrequency();
        cerr << "Image dimensions = [" << current.rows << " " << current.cols << "]" << endl;
        cerr << "Measurement - division by " << scale << ": time = " << secs << " seconds"<< endl;
    }

    cerr << endl << endl;
    cerr << "---------------------------------------------------------------------" << endl;
    cerr << "CPU-GPU PROCESSING on the original, 10 times" << endl;
    cerr << "---------------------------------------------------------------------" << endl;
    Mat original_cpu = hist.clone();

    // GPU processing
    int64 t0 = getTickCount();
    for(int i=0; i < 10; i++){
        // We first need to split the image and add them to a vector
        vector<Mat> data_gpu;
        data_gpu.push_back( original_cpu( Rect(0, 0, 4000, 4000) ).clone() );
        data_gpu.push_back( original_cpu( Rect(3999, 0, 4000, 4000) ).clone() );
        // Now process the data in a loop
        for(size_t j=0; j<data_gpu.size(); j++){
            cuda::GpuMat image_gpu(data_gpu[j]);
            cuda::GpuMat objbuf;
            cascade_gpu->detectMultiScale(image_gpu, objbuf);
            vector<Rect> detections;
            cascade_gpu->convert(objbuf, detections);
        }
    }
    int64 t1 = getTickCount();
    double secs = (t1-t0)/getTickFrequency();
    cerr << "GPU Measurement - [8000x4000] pixels: time = " << secs << " seconds"<< endl;

    return 0;
}

