TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += ../deps/include
DEPENDPATH += ../deps/include

QMAKE_LFLAGS += -Wl,--rpath=../deps/lib
LIBS += -L../deps/lib

LIBS += -pthread
LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_videoio -lopencv_objdetect -lopencv_cudaimgproc -lopencv_cudaobjdetect -lopencv_imgcodecs

copydata.commands = $(COPY_DIR) $$PWD/data $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

SOURCES += main.cpp
