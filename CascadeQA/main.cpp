#include <iostream>
#include <chrono>

#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>

#include <opencv2/core/cuda.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaobjdetect.hpp>

#define OPENCV_VIDEO_SOURCE "../../../Videos/Clips/001f92007bde-0_2014_06_18-07_36_52_831__15513_xvid.avi"
#define OPENCV_CASCADE_FILENAME "data/haarcascades_cuda/haarcascade_fullbody.xml"

using namespace cv;
using namespace std;

int main(int argc, char *argv[])
{
    VideoCapture videoSource;

    videoSource.open(OPENCV_VIDEO_SOURCE);

    if (!videoSource.isOpened()) {
        cerr << "Can not open video source";
        return -1;
    }

    cout << "Cuda device:" << endl;
    cuda::printShortCudaDeviceInfo(cv::cuda::getDevice());
    cout << endl;

    std::vector<cv::Rect> found;

    // Prepare cpu part
    cv::CascadeClassifier cpuCascade;
    cpuCascade.load(OPENCV_CASCADE_FILENAME);
    if (cpuCascade.empty()) {
        cerr << "Can't load cpu cascade from file" << OPENCV_CASCADE_FILENAME << endl;
        return -1;
    }
    namedWindow("CPU",1);

    //Prepare gpu part
    cv::Ptr<cv::cuda::CascadeClassifier> gpuCascade =
            cv::cuda::CascadeClassifier::create(OPENCV_CASCADE_FILENAME);
    if(gpuCascade->empty()) {
        cerr << "Can't load gpu cascade from file" << OPENCV_CASCADE_FILENAME << endl;
        return -1;
    }
    gpuCascade->setScaleFactor(1.01);
    gpuCascade->setMinNeighbors(1);
    gpuCascade->setMinObjectSize(Size());
    gpuCascade->setMaxObjectSize(Size());
    namedWindow("GPU",1);
    while(1)
    {
        Mat frame;
        if ( !videoSource.read(frame) ) {
            cerr << "Can not read frame from video source";
            return -1;
        }

        // CPU code
        Mat cpuFrame = frame.clone();
        found.clear();

        auto start = chrono::high_resolution_clock::now();

        cv::Mat grayFrame;
        cv::cvtColor(cpuFrame, grayFrame, cv::COLOR_BGR2GRAY);

        cpuCascade.detectMultiScale(grayFrame, found, 1.1, 3, 0, Size(), Size());

        auto cpuMs = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count();

        for (cv::Rect r : found) {
            r.x += cvRound(r.width*0.1);
            r.width = cvRound(r.width*0.8);
            r.y += cvRound(r.height*0.06);
            r.height = cvRound(r.height*0.9);
            cv::rectangle(cpuFrame, r.tl(), r.br(), cv::Scalar(0, 255, 0), 2);
        }
        imshow("CPU", cpuFrame);

        // GPU code
        Mat gpuFrame = frame.clone();
        found.clear();

        start = chrono::high_resolution_clock::now();
        cv::cuda::GpuMat gpuMat, grayMat, gpuFound;

        // upload time
        start = chrono::high_resolution_clock::now();
        gpuMat.upload(gpuFrame);
        auto uploadMs = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count();

        // convert time
        start = chrono::high_resolution_clock::now();
        cv::cuda::cvtColor(gpuMat, grayMat, cv::COLOR_BGR2GRAY);
        auto cvtMs = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count();

        // clean detection time
        start = chrono::high_resolution_clock::now();
        gpuCascade->detectMultiScale(grayMat, gpuFound);
        auto gpuMs = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count();

        // download time
        start = chrono::high_resolution_clock::now();
        gpuCascade->convert(gpuFound, found);
        auto downloadMs = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start).count();

        for (cv::Rect r : found) {
            r.x += cvRound(r.width*0.1);
            r.width = cvRound(r.width*0.8);
            r.y += cvRound(r.height*0.06);
            r.height = cvRound(r.height*0.9);
            cv::rectangle(gpuFrame, r.tl(), r.br(), cv::Scalar(0, 0, 255), 2);
        }
        imshow("GPU", gpuFrame);

        // Elapsed
        cout << "CPU: " << cpuMs << endl;
        cout << "GPU: " << uploadMs+cvtMs+gpuMs+downloadMs <<
                " | upload: " << uploadMs <<
                " convert: " << cvtMs <<
                " detect: " << gpuMs <<
                " download: " << downloadMs << endl;

        if (waitKey(1) >= 0) {
            break;
        }
    }

    return 0;
}
