#!/bin/bash

function printLine {
    printf '%80s\n' | tr ' ' $1
}

printLine '#'

tempInstallPath="install"

rm -rf $tempInstallPath
mkdir $tempInstallPath
cd $tempInstallPath
tempInstallPath=$(pwd)
cd ..
echo "Temp install path is:"
echo $tempInstallPath

printLine '#'

# Get the dependencies (Ubuntu Server or headless users):
sudo apt-get update
sudo apt-get -y install build-essential git cmake yasm libgtk2.0-dev libx264-dev cuda

printLine '#'

# Build OpenCV
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$tempInstallPath/usr/local/lib/
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$tempInstallPath/usr/local/lib/pkgconfig/:/usr/lib/x86_64-linux-gnu/pkgconfig/:/usr/lib/arm-linux-gnueabihf/pkgconfig/:/usr/share/pkgconfig/
export PKG_CONFIG_LIBDIR=$PKG_CONFIG_LIBDIR:$tempInstallPath/usr/local/lib/:/usr/lib/x86_64-linux-gnu/:/usr/lib/arm-linux-gnueabihf/

rm -rf opencv
git clone --depth 1 https://github.com/Itseez/opencv.git
cd opencv
mkdir build
cd build
cmake -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF -DCMAKE_BUILD_TYPE=RELEASE -DENABLE_FAST_MATH=ON -DWITH_CUDA=ON -DWITH_TBB=ON -DWITH_CUBLAS=ON -DCUDA_FAST_MATH=ON ..
make -j 4
make install DESTDIR=$tempInstallPath
cd ..
cd ..

printLine '#'

# Install
echo "Install"
rm -rf ./lib
cp -a $tempInstallPath/usr/local/lib ./lib
rm -rf ./lib/pkgconfig
rm -rf ./include
cp -a $tempInstallPath/usr/local/include ./include

printLine '#'

read -p "Clean? (y/n)" -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    rm -rf opencv
    rm -rf $tempInstallPath
fi

echo "Done"
